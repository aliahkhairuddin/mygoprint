import * as React from "react";
import "./footer.scss";
import { facebookIcon, twitterIcon, googleIcon } from "../../../constants/constant";


export class Footer extends React.Component {
    render() {
        return (
            <div className="contactDetails">
                <div className="firstDiv">
                    <div>
                        <span>GOPrint</span>
                        <hr />
                    </div>
                    <table>
                        <tr>
                            <td>
                                <img src="" alt="" />
                            </td>
                            <td>
                                <p>2499 Bee Street, Bear Lake</p>
                                <p>Michigan 49614</p>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>012-2571613</td>
                        </tr>
                        <tr>
                            <td>
                                <img src="" alt="" />
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                <div className="secondDiv">
                    <div>2019 GoPrint. All Rights Reserved.</div>
                    <div className="socialMedia">
                        <img src={facebookIcon} alt="" />
                        <img src={twitterIcon} alt="" />
                        <img src={googleIcon} alt="" />
                    </div>
                </div>
            </div>
        )
    }
}