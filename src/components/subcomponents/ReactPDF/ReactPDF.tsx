import * as React from 'react';
import * as ReactDoc from 'react-pdf';
import { Document } from 'react-pdf/dist/entry.webpack';
import { pdfjs } from 'react-pdf';
 
interface ReactPDFProps {
    file: any;
}

export class ReactPDF extends React.Component<ReactPDFProps> {
  state = {
    numPages: null as number,
    pageNumber: 1,
  }
 
  onDocumentLoadSuccess = ( numPages: any ) => {
    this.setState({ numPages });
  }
 
  render() {
    const { pageNumber, numPages } = this.state;
    const { file } = this.props;
 
    return (
      <div>
        <Document
          file={file}
          onLoadSuccess={this.onDocumentLoadSuccess}
        >
          <ReactDoc.Page pageNumber={pageNumber} />
        </Document>
        <p>Page {pageNumber} of {numPages}</p>
      </div>
    );
  }
}