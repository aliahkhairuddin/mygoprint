import * as React from "react";
import { Product, ProductOrder, User } from "../../../constants/interfaces";
import * as _ from "lodash";
import { Modal } from "../Modal/Modal";
import { observer } from "mobx-react";
import MicroModal from "micromodal";
import FileDownload from "js-file-download";
import "bootstrap/dist/js/bootstrap.bundle";
import {
  exampleProductSubmissionForm,
  exampleProducts
} from "../../../constants/constant";
import { userStore, orderStore } from "../../../stores/APIservice";
import { observable, runInAction, action, toJS } from "mobx";
import * as emailjs from "emailjs-com";
import { Redirect } from "react-router";
import { toast } from "react-toastify";
import "./adminTable.scss";

@observer
export class AdminTable extends React.Component {
  state = {
    totalPrice: 0
  };

  componentDidMount() {
    userStore.fetchUsers().then(response => {
      console.log("all users:", response);
    });
    userStore.fetchAdmins().then(response => {
      console.log('all admins', response);
    })

    // saves orders to variable allDocuments
    orderStore.getAllOrders().then(response => {
      console.log("all orders:", response);
      runInAction(() => {
        this.allDocuments = response;
      });
    });
  }

  @observable
  allDocuments: any;

  @observable
  currentOrder: ProductOrder = null;

  @action
  editPrice = async (order: ProductOrder) => {
    this.currentOrder = order;
    await MicroModal.show('editPrice', { awaitCloseAnimation: true });
  };

  @action
  downloadFile = async (file: any) => {
    const data = toJS(file);
    console.log('data wanting to download', data);
    await orderStore.downloadFile(data.id).then(({ data }) => {
      console.log('data', data, {data});
      const downloadUrl = window.URL.createObjectURL(new Blob([data]));
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', 'your_order.pdf'); //any other extension
      document.body.appendChild(link);
      link.click();
      link.remove();
    });
  }

  @action
  handleEditPrice = (event: any) => {
    const value: number = parseFloat(event.target.value);
    console.log("edit price value ", value);
    this.setState({
      totalPrice: value
    });
  };

  @action
  submitEditPriceForm = async () => {
    const data = this.currentOrder;
    const order = {
      orderId: data.orderId,
      fileName: data.fileName,
      orderFile: data.orderFile,
      typeOfBinding: data.typeOfBinding,
      coverPage: data.coverPage,
      others: data.others,
      totalPrice: this.state.totalPrice,
      orderStatus: data.orderStatus,
      username: data.user_name,
      userId: data.userId
    };
    await orderStore
      .updatePrintForm(order)
      .then(response => {
        toast.success("Order successfully updated!", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      })
      .catch(response => {
        toast.error("Error while updating order.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
    await orderStore.getAllOrders().then(response => {
      this.allDocuments = response;
    });
  };

  @action
  completeOrder = async (order: ProductOrder) => {
    this.currentOrder = order
    await MicroModal.show('completeOrder', { awaitCloseAnimation: true });
  };

  @action
  updateCompleteOrder = async () => {
    const data = toJS(this.currentOrder);
    const userID = data.userId;
    const orderID = data.orderId;
    const userData: User = await userStore.fetchUsers().then(response => {
      return _.find(response, (item: User, index) => {
        return item.userId === userID;
      });
    });
    const customer_email = userData.userEmail;
    const customer_name = userData.usersName;
    const customer_bill = data.totalPrice;

    await orderStore
      .completedOrder(orderID)
      .then(response => {
        console.log("order successfully completed");
      })
      .catch(response => {
        toast.error("Error while completing order.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
    await orderStore.getAllOrders().then(response => {
      this.allDocuments = response;
    });
    emailjs
      .send(
        "default_service", // default email provider in your EmailJS account
        "template_3IVOmBZg",
        {
          msu_printing_shop: "msuprintingshop@gmail.com",
          customer_name: customer_name,
          customer_email: customer_email,
          customer_bill: customer_bill,
          order_id: orderID
        },
        "user_nDsXftWRmUIjZUMxhndjz"
      )
      .then(response => {
        toast.success("Order completed! Customer has been notified.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      })
      // Handle errors here however you like, or use a React error boundary
      .catch(err => {
        toast.error("Error while sending email notification to customer.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
  };

  render() {
    const { totalPrice } = this.state;
    return (
      <div className="w-100 d-flex flex-column align-items-center adminTable">
        <div className="text-left w-85">
          <h2>All of your customers' orders are listed here.</h2>
        </div>
        <table className="table table-bordered w-85 text-center mt-4">
          <thead className="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Order ID</th>
              {/* <th scope="col">File Name</th> */}
              <th scope="col">Type of Binding</th>
              <th scope="col">Cover Page Remarks</th>
              <th scope="col">Other Remarks</th>
              <th scope="col">Total Price</th>
              <th scope="col">Order Status</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            {this.allDocuments &&
              _.map(this.allDocuments, (item: ProductOrder, index) => {
                return (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{item.orderId}</td>
                    {/* <td>{item.fileName}</td> */}
                    <td>{item.typeOfBinding}</td>
                    <td>{item.coverPage}</td>
                    <td>{item.others}</td>
                    <td>{item.totalPrice && item.totalPrice.toFixed(2)}</td>
                    <td>{item.orderStatus}</td>
                    <td className="d-flex justify-content-center">
                      <div className="dropdown show">
                        <a
                          className="btn btn-secondary dropdown-toggle"
                          href="#"
                          role="button"
                          id="dropdownMenuLink"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        ></a>
                        <div
                          className="dropdown-menu"
                          aria-labelledby="dropdownMenuLink"
                        >
                          <button
                            className="btn btn-primary dropdown-item"
                            data-micromodal-trigger="editPrice"
                            onClick={() => this.editPrice(item)}
                          >
                            Edit Price
                          </button>
                          <button className="btn btn-primary dropdown-item" onClick={() => { this.downloadFile(item) }}>
                            Download
                          </button>
                          <button
                            className="btn btn-primary dropdown-item"
                            data-micromodal-trigger="completeOrder"
                            onClick={async () => await this.completeOrder(item)}
                          >
                            Complete Order
                          </button>
                        </div>
                      </div>
                    </td>
                  </tr>
                );
              })}
            <Modal
              className="dropdown-item"
              modalId={`editPrice`}
              modalTitle={`Edit Price`}
              buttonText={`Edit Price`}
              showButton={false}
              saveButton={() => {
                this.submitEditPriceForm();
              }}
            >
              <p>
                Please input the total price for order ID{" "}
                {this.currentOrder && this.currentOrder.orderId}
              </p>
              <form>
                <div className="form-group">
                  <label>Price</label>
                  <input
                    type="number"
                    className="form-control"
                    id="orderPrice"
                    placeholder="Enter total price"
                    value={totalPrice}
                    onChange={this.handleEditPrice}
                  />
                </div>
              </form>
            </Modal>
            <Modal
              className="dropdown-item"
              modalId={`completeOrder`}
              modalTitle={`Complete Order`}
              buttonText={`Complete`}
              showButton={false}
              saveButton={() => {
                this.updateCompleteOrder();
              }}
            >
              Are you sure you want to complete this order?
            </Modal>
          </tbody>
        </table>
      </div>
    );
  }
}
