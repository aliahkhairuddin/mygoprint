import * as React from "react";
import MicroModal from "micromodal";
import "./modal.scss";

// if showButton isfalse, no need to put buttonText, inTable prop
interface ModalProps {
    modalId: string;
    modalTitle: string;
    buttonText?: string;
    saveButton?: any; // pass in function to determine what the save button does
    data?: any;
    inTable?: boolean;
    className?: string;
    showButton?: boolean;
    showContinueButton?: boolean;
}

export class Modal extends React.Component<ModalProps> {

    static defaultProps = {
        showButton: true,
        showContinueButton: true
    }

    render() {

        const { modalId, modalTitle, buttonText, saveButton, children, data, inTable, className, showButton, showContinueButton } = this.props;

        return (
            <>
                {
                    showButton &&
                    <div className={`d-flex ${inTable ? 'justify-content-center' : 'justify-content-end w-85'} ${className}`}>
                        <button className="btn btn-primary mx-1" data-micromodal-trigger={modalId} onClick={async () => await MicroModal.init(
                            { awaitCloseAnimation: true })}>{buttonText}</button>
                    </div>
                }

                <div className="modal micromodal-slide" id={modalId} aria-hidden="true">
                    <div className="modal__overlay" data-micromodal-close>
                        <div className="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
                            <header className="modal__header">
                                <h2 className="modal__title" id="modal-1-title">
                                    {modalTitle}
                                </h2>
                                <button className="modal__close" aria-label="Close modal" data-micromodal-close></button>
                            </header>
                            <main className="modal__content" id="modal-1-content">
                                <p>{children}</p>
                            </main>
                            <footer className="modal__footer">
                                {showContinueButton && <button className="modal__btn modal__btn-primary" data-micromodal-close onClick={() => saveButton()}>Continue</button>}
                                <button className="modal__btn" data-micromodal-close aria-label="Close this dialog window">Close</button>
                            </footer>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}