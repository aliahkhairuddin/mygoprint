import * as React from "react";
import { observer } from "mobx-react";
import * as _ from "lodash";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import "./pageBase.scss";
import { IntroPage } from "../pages/IntroPage/IntroPage";
import { Home } from "../pages/Home/Home";
import { Login } from "../pages/Login/Login";
import { Footer } from "./subcomponents/Footer/Footer";
import { Products } from "../pages/Product/Products";
import { userStore } from "../stores/APIservice";
import { observable, runInAction } from "mobx";
import { CustomerViewOrderList } from "../pages/CustomerViewOrderList/CustomerViewOrderList";

@observer
export class PageBase extends React.Component {
  componentDidMount() {
    const NavButtons = document.querySelectorAll("li");
    NavButtons.forEach((link, index) => {
      link.style.animation = `float-in ${index + 1}s ease-in-out`;
    });
    // console.log('did update');
    // runInAction(() => { this.isLogin = userStore.getLoginValue })
  }

  componentDidUpdate() { }

  render() {
    if (!_.isNull(sessionStorage.getItem('username'))) {
      runInAction(() => userStore.isLogin = true);
    }
    return (
      <>
        <Router>
          <div className="navigation-bar">
            <div className="navigation-top">
              <ul className="navigation-buttons d-flex justify-content-end align-items-center text-center text-uppercase">
                {/* <li>
                  <Link to="/home">Home</Link>
                </li> */}
                <li>
                  <Link to="/about">About</Link>
                </li>
                {userStore.getLoginValue && (
                  <li>
                    <Link to={
                      sessionStorage.getItem('role') === 'Admin' ? '/order-list' : '/print'
                    }>{sessionStorage.getItem('role') === 'Admin' ? 'Order List' : 'Print'}</Link>
                  </li>
                )}
                {userStore.getLoginValue && sessionStorage.getItem('role') === 'Normal' && (
                  <li>
                    <Link to="/your-orders">Your Orders</Link>
                  </li>
                )}
                {/* <li>
                  <Link to="/contact">Contact</Link>
                </li> */}
                {!userStore.getLoginValue && (
                  <li>
                    <Link to="/login">Register/Login</Link>
                  </li>
                )}

                {userStore.getLoginValue && (
                  <li>
                    <Link to="/logout">Logout</Link>
                  </li>
                )}
              </ul>
            </div>
          </div>
          <div className="content d-flex flex-column align-items-center justify-content-center">
            {!userStore.isLogin && (
              <Redirect
                to={{
                  pathname: "/login"
                }}
              />
            )}
            <Switch>
              <Route path={["/", "/home"]} exact={true} component={Home} />
              <Route path="/about" exact={true} component={IntroPage} />
              <Route path="/login" exact={true} component={Login} />
              <Route path={
                sessionStorage.getItem('role') === 'Admin' ? '/order-list' : '/print'
              } exact={true} component={Products} />
              <Route
                path="/your-orders"
                exact={true}
                component={CustomerViewOrderList}
              />
              <Route path="/logout" exact={true} component={Login} />
            </Switch>
            <Footer />
          </div>
        </Router>
      </>
    );
  }
}
