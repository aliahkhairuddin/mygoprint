import axios from 'axios';
import { User, Product, ProductOrder, UserLogin, EditProductOrder } from '../constants/interfaces';
import { computed, observable } from 'mobx';
import FileDownload from "js-file-download";

const API_BASE_URL = 'http://localhost:8080';

// change values in the interfaces at file path \online-printing-system\src\constants\interfaces.ts
// if backend API's JSON response has more / less fields
// reference : https://www.devglan.com/react-js/spring-boot-reactjs-crud-example

class UserStore {

    @observable
    isLogin: boolean = false;

    @computed
    get getLoginValue() {
        return this.isLogin;
    }

    // users-related API
    fetchUsers() {
        return axios.get(`${API_BASE_URL}/getUsers/r`).then(response => {
            return response.data;
        });
    }

    fetchAdmins() {
        return axios.get(`${API_BASE_URL}/getAdmin/r`).then(response => {
            return response.data;
        })
    }

    login(user: UserLogin) {
        // pass in user_id, user_password
        return axios.post(`${API_BASE_URL}/postUser/r`, { userId: user.userId, user_password: user.user_password });
    }

    logout() {
        return axios.post(`${API_BASE_URL}/postUser/r`);
    }

    register(user: User) {
        // pass in user_password, user_confirm_pass, usersName, userEmail
        return axios.post(`${API_BASE_URL}/postUser/r`, user);
    }
}

class OrderStore {
    submitPrintForm(print: ProductOrder) {
        return axios.post(`${API_BASE_URL}/postOrder/r`, print);
    }

    uploadFile(file: any) {
        return axios.post(`${API_BASE_URL}/uploadFile`, file);
    }

    downloadFile(fileName: any) {
        return axios.get(`${API_BASE_URL}/downloadFile/${fileName}`, {responseType: 'blob'})
    }

    updatePrintForm(print: EditProductOrder) {
        return axios.post(`${API_BASE_URL}/updateOrder/r`, print);
    }

    downloadDocument(fileId: number) {
        return axios.get(`${API_BASE_URL}/getOrders/r/${fileId}`).then(response => {
            return response.data;
        });;
    }

    getAllOrders() {
        return axios.get(`${API_BASE_URL}/getAllOrders/r`).then(response => {
            return response.data;
        });;
    }

    getOrderStatus(userId: number) {
        return axios.get(`${API_BASE_URL}/getUserOrders/r/${userId}`).then(response => {
            return response.data;
        });;
    }

    sendNotification(userName: string, orderId: number) {
        return axios.post(`${API_BASE_URL}/getAllOrders/r`, { username: userName, order_id: orderId });
    }

    completedOrder(orderId: number) {
        return axios.get(`${API_BASE_URL}/change/${orderId}`);
    }
}


export const userStore = new UserStore();
export const orderStore = new OrderStore();