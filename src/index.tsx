import * as React from "react";
import * as ReactDOM from "react-dom";
import './index.scss';

import Main from "./pages/Main/Main";

ReactDOM.render(
  <Main/>,
  document.getElementById("root")
);
