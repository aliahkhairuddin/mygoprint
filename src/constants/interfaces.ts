
export interface User {
    userId?: string | number;
    username: string;
    user_password: string;
    user_confirm_pass: string;
    usersName: string;
    userEmail: string;
}

export interface Admin {
    adminId: number;
    staffId: string;
    staffPass: string;
}

export interface UserLogin {
    userId: string | number;
    user_password: string;
}

export interface Product {
    id: string | number;
    name: string;
    price: number;
    priceLabel: string;
}

export interface ProductOrder {
    orderId?: number;
    fileName: any;
    id: string,
    orderFile?: any;
    typeOfBinding: string;
    coverPage: string;
    others: string;
    totalPrice?: number;
    orderStatus?: string;
    userId: number;
    user_name?: string;
}

export interface EditProductOrder {
    orderId: number;
    totalPrice?: number;
}