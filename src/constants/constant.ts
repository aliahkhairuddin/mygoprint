import { Product, ProductOrder } from "./interfaces";
import { observable } from "mobx";

/* images */

export const developerProfilePicture = require('../../public/images/daria_nepriakhina_i5iihhsatp4_unsplash.png');
export const facebookIcon = require('../../public/images/icons8_facebook.png');
export const twitterIcon = require('../../public/images/icons8_twitter.png');
export const googleIcon = require('../../public/images/icons8_google_plus.png');
// export const startPrintingBanner = require('../../public/images/printing.png');

/* fake data */
export const exampleProducts: Product[] = observable([
    {
        id: 1,
        name: 'Comb Bind',
        price: 2,
        priceLabel: 'RM2.00'
    },
    {
        id: 2,
        name: 'Tape Bind',
        price: 1.5,
        priceLabel: 'RM1.50'
    },
    {
        id: 3,
        name: 'No Bind',
        price: 0.2,
        priceLabel: 'RM0.20'
    },

])

export const exampleProductSubmissionForm: ProductOrder[] = [
    {
        // orderId: 2,
        fileName: null,
        id: '1',
        // orderFile: null,
        typeOfBinding: "test",
        coverPage: "test",
        others: "test",
        // totalPrice: 2,
        // orderStatus: "A",
        userId: 1,
        user_name: 'test'
    },
    // {
    //     orderId: 2,
    //     fileName: null,
    //     orderFile: null,
    //     typeOfBinding: "test",
    //     coverPage: "test",
    //     others: "test",
    //     totalPrice: 2,
    //     orderStatus: "A",
    //     userId: 1,
    // },
    // {
    //     orderId: 2,
    //     fileName: null,
    //     orderFile: null,
    //     typeOfBinding: "test",
    //     coverPage: "test",
    //     others: "test",
    //     totalPrice: 2,
    //     orderStatus: "A",
    //     userId: 1,
    // },
    // {
    //     orderId: 2,
    //     fileName: null,
    //     orderFile: null,
    //     typeOfBinding: "test",
    //     coverPage: "test",
    //     others: "test",
    //     totalPrice: 2,
    //     orderStatus: "A",
    //     userId: 1,
    // },
    // {
    //     orderId: 2,
    //     fileName: null,
    //     orderFile: null,
    //     typeOfBinding: "test",
    //     coverPage: "test",
    //     others: "test",
    //     totalPrice: 2,
    //     orderStatus: "A",
    //     userId: 1,
    // }
]
