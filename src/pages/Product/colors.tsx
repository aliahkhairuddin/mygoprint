export const color = [
         {
           name: "Warm Flame 1",
           background:
             "linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%)"
         },
         {
           name: "Night Fade 1",
           background:
             "linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%)"
         },
         {
           name: "Spring Warmth 1",
           background:
             "linear-gradient(to top, #fad0c4 0%, #ffd1ff 100%)"
         },
         {
           name: "Warm Flame 2",
           background:
             "linear-gradient(45deg, #ff9a9e 0%, #fad0c4 99%, #fad0c4 100%)"
         },
         {
           name: "Night Fade 2",
           background:
             "linear-gradient(to top, #a18cd1 0%, #fbc2eb 100%)"
         },
         {
           name: "Spring Warmth 2",
           background:
             "linear-gradient(to top, #fad0c4 0%, #ffd1ff 100%)"
         },
         {
           name: "Spring Warmth 2",
           background:
             "linear-gradient(to top, #fad0c4 0%, #ffd1ff 100%)"
         }
       ];
