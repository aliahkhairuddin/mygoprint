import * as React from "react";
import * as _ from "lodash";
import { color } from "./colors";
import "./products.scss";
import { exampleProducts } from "../../constants/constant";
import { userStore, orderStore } from "../../stores/APIservice";
import { AdminTable } from "../../components/subcomponents/Table/AdminTable";
import { observer } from "mobx-react";
import { observable, action } from "mobx";
import { toast } from "react-toastify";
import MicroModal from "micromodal";
import { Modal } from "../../components/subcomponents/Modal/Modal";
// const rowNumber = Math.round(color.length / 3);
// let rowNumber = 0;
@observer
export class Products extends React.Component {
  state = {
    // orderId: 1,
    fileName: "",
    orderFile: null as any,
    typeOfBinding: "",
    coverPage: "",
    others: "",
    totalPrice: 1,
    orderStatus: "A",
    userId: parseInt(sessionStorage.getItem('user_id')),
    // user_name: "test"
  };

  @observable
  isAdmin: boolean = true;

  @observable
  typeOfBindingOption: number;

  @action
  switchView = () => {
    this.isAdmin = !this.isAdmin;
    console.log("isAdmin :", this.isAdmin);
  };

  @action
  handleChange = (event: any) => {
    const value = event.target.value;
    this.setState({
      ...this.state,
      [event.target.name]: value
    });
    if (event.target.name === "fileName") {
      console.log("event target :", event.target);
      console.log("event target files :", event.target.files[0]);
      const file = event.target.files[0];
      const fileName = event.target.files[0].name;

      const formData = new FormData();
      formData.append('file', file);

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = e => {
        const fileData = e.target.result;
        this.setState({
          fileName: fileName,
          // orderFile: formData
          // orderFile: fileName
          orderFile: formData
        });
      };
    }
  };

  @action
  handleTypeOfBinding = (type: string, price: number, chosen: number) => {
    this.typeOfBindingOption = chosen;
    this.setState({
      typeOfBinding: type,
      totalPrice: price
    });
  };

  submitForm = async (e: any) => {
    e.preventDefault;
    console.log("states", this.state);
    if (this.state.fileName.split(".")[1] !== 'pdf') {
      toast.error("Please provide a pdf file. You can convert your file at the link provided.", {
        position: toast.POSITION.TOP_CENTER,
        className: "text-center",
        autoClose: 4000
      });
      await MicroModal.show('errorUpload', { awaitCloseAnimation: true });
    } else {
      await orderStore.uploadFile(this.state.orderFile).then(async (response) => {
        await orderStore
          .submitPrintForm({
            fileName: this.state.fileName,
            id: response.data.id,
            // orderFile: this.state.orderFile,
            typeOfBinding: this.state.typeOfBinding,
            coverPage: this.state.coverPage,
            others: this.state.others,
            // username: this.state.user_name
            userId: this.state.userId
          })
          .then((response: any) => {
            toast.success("Order submitted!", {
              position: toast.POSITION.TOP_CENTER,
              className: "text-center",
              autoClose: 2500
            });
          })
          .catch((response: any) => {
            toast.error("Error while submitting order.", {
              position: toast.POSITION.TOP_CENTER,
              className: "text-center",
              autoClose: 2500
            });
          });
      });
    }
  };

  render() {
    const {
      fileName,
      orderFile,
      typeOfBinding,
      coverPage,
      others,
      // totalPrice,
      orderStatus,
      // user_name
      userId
    } = this.state;

    return (
      <div className="products w-100">
        {sessionStorage.getItem('role') === 'Admin' ? (
          <AdminTable />
        ) : (
            <>
              <div className="banner">
                <p>Start Printing!</p>
              </div>
              <div className="easySteps">
                <div className="firstLine">
                  <p>4 Easy Steps To Print With GoPrint</p>
                  <hr />
                </div>
                <div className="circles">
                  <div>Upload File</div>
                  <hr />
                  <div>Set Specification</div>
                  <hr />
                  <div>Confirm Order</div>
                  <hr />
                  <div>Pay and Collect</div>
                </div>
              </div>
              {/* <form> */}
              <div className="upload">
                <div className="uploadText">1. Upload Your File</div>
                <div className="uploadFile">
                  <div className="chooseFile form-group">
                    <input
                      type="file"
                      className="form-control-file w-auto"
                      name="fileName"
                      accept="application/pdf"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>
              <div className="printing">
                <div className="specifyYourPrinting">
                  2. Specify Your Printing
              </div>
                <p>Type of Binding</p>
                <div className="bindingType">
                  {// uncomment this to test the API
                    // _.map(this.fetchProductsFromAPI, (item: any) => {
                    // return (
                    // <button className="btn">{item.name}</button>
                    // )
                    // })

                    // fake data from file path online-printing-system\src\constants\constant.ts
                    exampleProducts.map((item, index) => {
                      return (
                        <button
                          className={`btn mx-2 ${this.typeOfBindingOption === index && 'typeOfBindingOption'}`}
                          onClick={() => {
                            this.handleTypeOfBinding(item.name, item.price, index);
                          }}
                        >
                          <span className="text-nowrap">{item.name}</span>
                          <br />
                          <span>{item.priceLabel}</span>
                        </button>
                      );
                    })}
                </div>
                <p>Cover Page</p>
                <div className="coverPage form-group w-75">
                  <textarea
                    className="form-control"
                    name="coverPage"
                    value={coverPage}
                    onChange={this.handleChange}
                  />
                </div>
                <p>Others</p>
                <div className="others form-group w-75">
                  <textarea
                    className="form-control"
                    name="others"
                    value={others}
                    onChange={this.handleChange}
                  />
                </div>
                <button
                  className="btn btn-secondary btn-submit"
                  onClick={this.submitForm}
                >
                  Submit
              </button>
                <Modal
                  className="dropdown-item"
                  modalId={`errorUpload`}
                  modalTitle={`Error Upload`}
                  buttonText={`Close`}
                  showButton={false}
                  showContinueButton={false}
                >
                  <h5>If your file's format is other than PDF, please click <a href="http://www.zamzar.com" target="_blank">here</a> to convert it to PDF.</h5>
            </Modal>
              </div>
              {/* </form> */}
            </>
          )}
      </div>
    );
  }
}
