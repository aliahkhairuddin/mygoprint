import * as React from "react";
import "lodash";
import { hot } from "react-hot-loader";
import { PageBase } from "../../components/PageBase";
import "./main.scss";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// Call it once in your app. At the root of your app is the best place
toast.configure();
function Main() {
  return (
    <>
      <PageBase />
    </>
  );
}

export default hot(module)(Main);
