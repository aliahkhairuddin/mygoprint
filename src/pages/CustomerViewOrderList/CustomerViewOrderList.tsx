import * as React from "react";
import * as _ from "lodash";
import { observer } from "mobx-react";
import MicroModal from "micromodal";
import "bootstrap/dist/js/bootstrap.bundle";
import { observable, runInAction, action } from "mobx";
import * as emailjs from "emailjs-com";
import { Redirect } from "react-router";
import { toast } from "react-toastify";
import { userStore, orderStore } from "../../stores/APIservice";
import { Modal } from "../../components/subcomponents/Modal/Modal";
import { ProductOrder, User } from "../../constants/interfaces";
import "./CustomerViewOrderList.scss";

@observer
export class CustomerViewOrderList extends React.Component {
  state = {
    totalPrice: 0
  };

  componentDidMount() {
    runInAction(async () => {
      this.allDocuments = [];
      await orderStore.getAllOrders().then(response => {
        return _.map(response, (item: ProductOrder, index) => {
          if (!_.isNull(item) && item.userId === parseInt(sessionStorage.getItem('user_id'))) {
            this.allDocuments.push(item);
          }
        });
      });
    })
    console.log('fetch admins', this.allDocuments);
  }

  @observable
  allDocuments: any;

  @observable
  currentOrder: ProductOrder = null;

  @action
  editPrice = async (order: ProductOrder) => {
    this.currentOrder = order;
    await MicroModal.init({ awaitCloseAnimation: true });
  };

  @action
  handleEditPrice = (event: any) => {
    const value: number = parseFloat(event.target.value);
    console.log("edit price value ", value);
    this.setState({
      totalPrice: value
    });
  };

  @action
  submitEditPriceForm = async () => {
    const data = this.currentOrder;
    const order = {
      orderId: data.orderId,
      fileName: data.fileName,
      orderFile: data.orderFile,
      typeOfBinding: data.typeOfBinding,
      coverPage: data.coverPage,
      others: data.others,
      totalPrice: this.state.totalPrice,
      orderStatus: data.orderStatus,
      username: data.user_name
    };
    await orderStore
      .updatePrintForm(order)
      .then(response => {
        toast.success("Order successfully updated!", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      })
      .catch(response => {
        toast.error("Error while updating order.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
    await orderStore.getAllOrders().then(response => {
      this.allDocuments = response;
    });
  };

  @action
  completeOrder = async (order: ProductOrder) => {
    this.currentOrder = order;
    await MicroModal.init({ awaitCloseAnimation: true });
  };

  @action
  updateCompleteOrder = async () => {
    const user_name = this.currentOrder.user_name;
    const orderID = this.currentOrder.orderId;
    const userData: User = await userStore.fetchUsers().then(response => {
      return _.find(response, (item: User, index) => {
        return item.username === user_name;
      });
    });
    const customer_email = userData.userEmail;
    const customer_name = userData.usersName;
    const customer_bill = this.currentOrder.others;

    await orderStore
      .completedOrder(orderID)
      .then(response => {
        console.log("order successfully completed");
      })
      .catch(response => {
        toast.error("Error while completing order.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
    await orderStore.getAllOrders().then(response => {
      this.allDocuments = response;
    });
    emailjs
      .send(
        "default_service", // default email provider in your EmailJS account
        "template_3IVOmBZg",
        {
          msu_printing_shop: "msuprintingshop@gmail.com",
          customer_name: customer_name,
          customer_email: customer_email,
          customer_bill: customer_bill,
          order_id: orderID
        },
        "user_nDsXftWRmUIjZUMxhndjz"
      )
      .then(response => {
        toast.success("Order completed! Customer has been notified.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      })
      // Handle errors here however you like, or use a React error boundary
      .catch(err => {
        toast.error("Error while sending email notification to customer.", {
          position: toast.POSITION.TOP_CENTER,
          className: "text-center",
          autoClose: 2500
        });
      });
  };

  render() {
    const { totalPrice } = this.state;
    return (
      <div className="w-100 d-flex flex-column align-items-center customerView">
        <div className="text-left w-85">
          <h2>All of your orders are listed here.</h2>
        </div>
        <table className="table table-bordered w-85 text-center mt-4">
          <thead className="thead-light">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Order ID</th>
              {/* <th scope="col">File Name</th> */}
              <th scope="col">Type of Binding</th>
              <th scope="col">Cover Page Remarks</th>
              <th scope="col">Other Remarks</th>
              <th scope="col">Total Price</th>
              <th scope="col">Order Status</th>
              {/* <th scope="col">Actions</th> */}
            </tr>
          </thead>
          <tbody>
            {this.allDocuments &&
              _.map(this.allDocuments, (item: ProductOrder, index) => {
                return (
                  <tr>
                    <td>{index + 1}</td>
                    <td>{item.orderId}</td>
                    {/* <td>{item.fileName}</td> */}
                    <td>{item.typeOfBinding}</td>
                    <td>{item.coverPage}</td>
                    <td>{item.others}</td>
                    <td>{item.totalPrice && item.totalPrice.toFixed(2)}</td>
                    <td>{item.orderStatus}</td>
                  </tr>
                );
              })}
            <Modal
              className="dropdown-item"
              modalId={`editPrice`}
              modalTitle={`Edit Price`}
              buttonText={`Edit Price`}
              showButton={false}
              saveButton={() => {
                this.submitEditPriceForm();
              }}
            >
              <p>
                Please input the total price for order ID{" "}
                {this.currentOrder && this.currentOrder.orderId}
              </p>
              <form>
                <div className="form-group">
                  <label>Price</label>
                  <input
                    type="number"
                    className="form-control"
                    id="orderPrice"
                    placeholder="Enter total price"
                    value={totalPrice}
                    onChange={this.handleEditPrice}
                  />
                </div>
              </form>
            </Modal>
            <Modal
              className="dropdown-item"
              modalId={`completeOrder`}
              modalTitle={`Complete Order`}
              buttonText={`Complete`}
              showButton={false}
              saveButton={() => {
                this.updateCompleteOrder();
              }}
            >
              Are you sure you want to complete this order?
            </Modal>
          </tbody>
        </table>
      </div>
    );
  }
}
