import * as React from 'react';
import './introPage.scss';
import { Route } from 'react-router-dom';
import { Footer } from '../../components/subcomponents/Footer/Footer';
import { developerProfilePicture } from '../../constants/constant';

export class IntroPage extends React.Component {

    render() {
        return (
            <div className="introPage">
                <div className="introduction">
                    <p className="firstLine">At GoPrint, We Care</p>
                    <div className="secondLine">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed.</div>
                </div>
                <div className="easySteps">
                    <div className="firstLine">
                        <p>4 Easy Steps To Print With GoPrint</p>
                        <hr />
                    </div>
                    <div className="circles">
                        <div>Upload File</div>
                        <hr />
                        <div>Set Specification</div>
                        <hr />
                        <div>Confirm Order</div>
                        <hr />
                        <div>Pay and Collect</div>
                    </div>
                </div>
                <div className="whatIsGoPrint">
                    <p className="firstLine">What is GoPrint?</p>
                    <div className="secondLine">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed.</div>
                </div>
                <div className="developerProfile">
                    <div>
                        <img src={developerProfilePicture} alt="" />
                    </div>
                    <div className="developerProfileDetails">
                        <p className="firstLine">Aliah Nabilah</p>
                        <div className="secondLine">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren</div>
                    </div>
                </div>
            </div>
        )
    }
}