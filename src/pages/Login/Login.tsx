import * as React from "react";
import * as _ from "lodash";
import "./login.scss";
import { observer } from "mobx-react";
import { observable, action, runInAction } from "mobx";
import { User, Admin } from "../../constants/interfaces";
import { userStore } from "../../stores/APIservice";
import { Redirect } from "react-router";
import { toast } from "react-toastify";

const developerProfilePicture = require("../../../public/images/daria_nepriakhina_i5iihhsatp4_unsplash.png");
const facebookIcon = require("../../../public/images/icons8_facebook.png");
const twitterIcon = require("../../../public/images/icons8_twitter.png");
const googleIcon = require("../../../public/images/icons8_google_plus.png");

@observer
export class Login extends React.Component {
    state = {
        user_name: null as string,
        usersName: null as string,
        user_password: null as string,
        userEmail: null as string,
        user_confirm_pass: null as string
    };

    async componentDidMount() {
        console.log('users', await userStore.fetchUsers().then(response => { return response }));
    }

    @observable
    isRegister: boolean = true;

    @action
    toggleTab = (bool: boolean) => {
        this.isRegister = bool;
    };

    @action
    submitForm = async (e: any) => {
        e.preventDefault;
        if (this.isRegister) {
            let pass = this.state.user_password;
            let reg = new RegExp("(?=.*[A-Z])(?=.*[0-9])");
            let test = reg.test(pass);
            if (_.isNull(this.state.user_name) && _.isNull(this.state.user_password)) {
                toast.error("Please input username and password.", {
                    position: toast.POSITION.TOP_CENTER,
                    className: "text-center",
                    autoClose: 2500
                });
            }
            if (test) {
                await userStore
                    .register({
                        // userId: this.state.userId,
                        username: this.state.user_name,
                        usersName: this.state.usersName,
                        userEmail: this.state.userEmail,
                        user_password: this.state.user_password,
                        user_confirm_pass: this.state.user_confirm_pass
                    })
                    .then(response => {
                        toast.success("You have successfully registered an account!", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                    })
                    .catch(response => {
                        toast.error("Error while trying to register an account", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                    });
            } else {
                toast.error("Your password must contain at least 1 uppercase character and 1 numeric character", {
                    position: toast.POSITION.TOP_CENTER,
                    className: "text-center",
                    autoClose: 2500
                });
            }
        } else {
            if (_.isNull(this.state.user_name) && _.isNull(this.state.user_password)) {
                toast.error("Please input username and password.", {
                    position: toast.POSITION.TOP_CENTER,
                    className: "text-center",
                    autoClose: 2500
                });
            }
            else {
                const normalUserLoginData: User = await userStore.fetchUsers().then(response => {
                    return _.find(response, (item: User, index) => {
                        return item.username === this.state.user_name;
                    });
                });
                const adminLoginData: Admin = await userStore.fetchAdmins().then(response => {
                    return _.find(response, (item: Admin, index) => {
                        console.log('fetch admins', item.staffId, this.state.user_name);
                        return item.staffId === this.state.user_name;
                    });
                })
                if (_.isUndefined(normalUserLoginData) && _.isUndefined(adminLoginData)) {
                    toast.error("Sorry, user does not exist.", {
                        position: toast.POSITION.TOP_CENTER,
                        className: "text-center",
                        autoClose: 2500
                    });
                } else if (normalUserLoginData) {
                    if (
                        normalUserLoginData.username === this.state.user_name &&
                        normalUserLoginData.user_password === this.state.user_password
                    ) {
                        sessionStorage.setItem('username', `${normalUserLoginData.username}`)
                        sessionStorage.setItem('user_id', `${normalUserLoginData.userId}`)
                        sessionStorage.setItem('user_email', `${normalUserLoginData.userEmail}`)
                        sessionStorage.setItem('role', `Normal`)
                        toast.success("You are now logged in.", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                        runInAction(() => (userStore.isLogin = true));
                    } else {
                        toast.error("Wrong username / password entered.", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                    }
                } else if (adminLoginData) {
                    if (
                        adminLoginData.staffId === this.state.user_name &&
                        adminLoginData.staffPass === this.state.user_password
                    ) {
                        sessionStorage.setItem('username', `${adminLoginData.staffId}`)
                        sessionStorage.setItem('user_id', `${adminLoginData.adminId}`)
                        sessionStorage.setItem('role', `Admin`)
                        toast.success("You are now logged in.", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                        runInAction(() => (userStore.isLogin = true));
                    } else {
                        toast.error("Wrong username / password entered.", {
                            position: toast.POSITION.TOP_CENTER,
                            className: "text-center",
                            autoClose: 2500
                        });
                    }
                }
            }
        }
    };

    @action
    handleChange = (event: any) => {
        const value = event.target.value;
        this.setState({
            ...this.state,
            [event.target.name]: value
        });
    };

    render() {
        const {
            user_name,
            user_password,
            user_confirm_pass,
            usersName,
            userEmail
        } = this.state;
        if (window.location.pathname === "/logout") {
            sessionStorage.clear();
            runInAction(() => {
                userStore.isLogin = false;
            });
            console.log("you have been logged out");
        }
        return (
            <div className="loginPage">
                {userStore.isLogin && (
                    <Redirect
                        to={{
                            pathname: sessionStorage.getItem('role') === 'Admin' ? '/order-list' : '/print'
                        }}
                    />
                )}
                {!userStore.isLogin && (
                    <Redirect
                        to={{
                            pathname: "/login"
                        }}
                    />
                )}
                <div className="login">
                    <div className="authenticationContainer">
                        <div className="tabs">
                            <div
                                className={`${this.isRegister ? "active-tab" : ""}`}
                                onClick={() => {
                                    this.toggleTab(true);
                                }}
                            >
                                Sign Up
              </div>
                            <div
                                className={`${!this.isRegister ? "active-tab" : ""}`}
                                onClick={() => {
                                    this.toggleTab(false);
                                }}
                            >
                                Login
              </div>
                        </div>
                        <div className="fields">
                            {/* <form> */}
                            {this.isRegister ? (
                                <>
                                    <div className="form-group">
                                        <label>Username</label>
                                        <input
                                            name="user_name"
                                            onChange={this.handleChange}
                                            value={user_name}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Name</label>
                                        <input
                                            name="usersName"
                                            onChange={this.handleChange}
                                            value={usersName}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Email Address</label>
                                        <input
                                            type="email"
                                            name="userEmail"
                                            value={userEmail}
                                            onChange={this.handleChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Password</label>
                                        <input
                                            type="password"
                                            name="user_password"
                                            value={user_password}
                                            onChange={this.handleChange}
                                            className="form-control"
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Confirm Password</label>
                                        <input
                                            type="password"
                                            name="user_confirm_pass"
                                            value={user_confirm_pass}
                                            onChange={this.handleChange}
                                            className="form-control"
                                        />
                                    </div>
                                </>
                            ) : (
                                    <>
                                        <div className="form-group">
                                            <label>Username</label>
                                            <input
                                                name="user_name"
                                                onChange={this.handleChange}
                                                value={user_name}
                                                className="form-control"
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label>Password</label>
                                            <input
                                                name="user_password"
                                                type="password"
                                                onChange={this.handleChange}
                                                value={user_password}
                                                className="form-control"
                                            />
                                        </div>
                                    </>
                                )}

                            <div>
                                <button onClick={this.submitForm} className="btn btn-orange">
                                    {this.isRegister ? "Register" : "Login"}
                                </button>
                            </div>
                            {/* </form> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
